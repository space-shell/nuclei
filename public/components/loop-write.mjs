const styles =
  `
    <style>
      :host {
        white-space: nowrap;
        pointer-events: none;
        overflow: hidden;
      }

      :host > span {
        position: relative;
        display: inline-block;
      }

      :host > span:nth-of-type(1) {
      }

      :host > span:nth-of-type(2) {
      }
    </style>
  `

const template = ({ ctx }) =>
  Array.from({ length: 2 }).map(elem =>
    `
      <span>
        ${ ctx.textContent.repeat(1) }
      </span>
    `.trim()
  ).join('')

export default class LoopWrite extends HTMLElement {
	constructor (...args) {
		super (...args)

    this.$ = Object.assign({
      duration: 2500,
      speed: 0
    }, this.dataset)

    this.$.shadow = this.attachShadow({ mode: 'closed' })

    this.$.shadow
      .innerHTML =
        `
          ${styles}

          ${template({ ctx: this })}
        `
	}
  
  static get observedAttributes () {
    return [
      'data-duration'
    ]
  }

  get velocity () {
    if (!this.$.speed)
      return this.$.duration

    return Math.round((this.$.shadow.lastElementChild.offsetWidth / this.$.speed) * 1000)
  }

	connectedCallback () {
    const awaitLoop = retry => {
      if ( !retry || !this.$.shadow.lastElementChild.offsetWidth )
        return window.requestAnimationFrame(() => awaitLoop(retry - 1))

      console.log(this.velocity)

      this.$.shadow.querySelectorAll('span')
        .forEach((span, idx) => {
          span.animate([
            { transform: `translateX(0)` },
            { transform: `translateX(-100%)` }
          ], {
            duration: parseInt(this.velocity),
            iterations: Infinity
          })
        })
    }
    
    if (this.$.speed)
      awaitLoop(10)
    else
      awaitLoop(0)
	}
}

customElements.define('loop-write', LoopWrite)
