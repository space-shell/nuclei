const style =
  `
    <style>
      :host {
        display: flex;
        justify-content: space-between;
      }

      slot[name=left], slot[name=right] {
        text-orientation: sideways;
        text-align: center;
      }

      slot[name=left]{
        writing-mode: sideways-lr;
      }

      slot[name=right]{
        writing-mode: sideways-rl;
      }
    </style>
  `

const template = ({}) =>
  `
    ${style}

    <slot name=left>
    </slot>
    
    <slot>
    </slot>

    <slot name=right>
    </slot>
  `

customElements.define('side-text', class extends HTMLElement {
    constructor(...args) {
      super(...args)

      this.attachShadow({ mode: 'closed' })
        .innerHTML = template({})

  }
})
