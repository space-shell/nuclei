// NOTE - JN - Possibly CSS3 Variable
const POSITIONS = {
  top:
    `
      left: 0;
      top: 0;
      transform: translate(-50%, calc(-1rem - 100%));
    `,
  right:
    `
      left: 100%;
      top: 0;
      transform: translate(1rem, -50%);
    `,
  bottom:
    `
      top: 100%;
      left: 0;
      transform: translate(-50%, 1rem);
    `,
  left:
    `
      top: 0;
      left: 0;
      transform: translate(calc(-1rem - 100%), -50%);
    `
}

const style = ({ position }) =>
  `
    <style>
      :host {
        position: relative;
        cursor: pointer;
      }

      :host section {
        opacity: 0;
        position: absolute;
        pointer-events: none;

        ${POSITIONS[position]}
      }

      :host([data-open]) section {
        pointer-events: auto;
      }
    </style>
  `

const template = ({
  position
}) =>
  `
    ${ style({ position }) }

    <slot name=button>
      <button>
        Popup
      </button>
    </slot name=button>

    <section>
      <slot>
      </slot>
    </section>
  `

customElements.define( 'tooltip-popup', class extends HTMLElement {
  constructor ( ...args ) {
    super ( ...args )

    this.attachShadow({ mode: 'open' })
      .innerHTML = template( this.staticAttributes )
  }

  get wrapperElem () {
    return this.shadowRoot.querySelector( 'section' )
  }

  get triggerElem () {
    return this.shadowRoot.querySelector( 'slot[name=button]' )
  }

  get staticAttributes () {
    const {
      position = 'top'
    } = this.dataset

    return {
      position
    }
  }

  static get observedAttributes () {
    return [
      'data-open'
    ]
  }

  attributeChangedCallback (name, prev, cur) {
    this.setOpen(cur)
  }

  setOpen (open) {
    this.wrapperElem.animate([
      { opacity: open ? 0 : 1 },
      { opacity: open ? 1 : 0 }
    ], {
      duration: 250,
      fill: 'forwards'
    })
  }

  handleClick ( evt ) {
    console.log(this.dataset.open)

    if (this.dataset.open)
      delete this.dataset.open
    else
      this.dataset.open = true
  }

  connectedCallback () {
    this.triggerElem.addEventListener( 'click', () => this.handleClick() )
  }
})


