import { bind, wire } from 'https://cdn.jsdelivr.net/npm/hyperhtml/esm.js'

const style = ({}) => wire()
  `
    <style>
      :host {
        overflow: hidden;
      }

      :host, img, article {
        height: 100%;
        width: 100%;
      }

      img, article {
        object-fit: cover;
      }

      :host slot {
        position: absolute;
        bottom: 0;
        left: 0;
      }

      :host #message {
        transition:
          transform 160ms ease;
      }
      
      :host(:hover) #message {
        transform: translateY(-100%);
      }
    </style>
  `

customElements.define(
  'hover-card',
  class extends HTMLElement {
    constructor(...args) {
      super(...args)

      this.$ = Object.assign({
        image: '',
        href: ''
      }, this.dataset)

      this.template = bind(
        this.attachShadow({ mode: 'closed' })
      )
    }

    mouseOver (evt) {
      // console.log(evt)
    }

    render ({ image, href }) {
      this.template
        `
          ${style({})}

          <article
            onclick=${ evt => location.href = href }
            onmouseover=${this.mouseOver}>

            <picture>
              <img src=${image}>
            </picture>

            <div id=message>
              <slot>
                Click to see more
              </slot>
            </div>
          <article>
        `
    }

    connectedCallback () {
      this.render(this.$)
    }
  })
