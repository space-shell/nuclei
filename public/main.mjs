import { wire, bind } from 'https://cdn.jsdelivr.net/npm/hyperhtml/esm.js'

import * as templ from '/main-template.mjs'

const MODULE_SCRIPTS = document.head.querySelectorAll('script[type=module]')

const fileName = url => url.split('/').pop().split('.').shift()

const PAGE_LAYOUT = {
  compsInput: document.body.querySelector('#comps-input'),
  stateProps: document.body.querySelector('#state-props'),
  reactiveProps: document.body.querySelector('#reactive-props'),
  eventLogs: document.body.querySelector('#event-logs'),
  mainContent: document.body.querySelector('body main')
}

const PAGE_LAYOUT_BOUND = {
  compsInput: bind(PAGE_LAYOUT.compsInput),
  stateProps: bind(PAGE_LAYOUT.stateProps),
  reactiveProps: bind(PAGE_LAYOUT.reactiveProps),
  eventLogs: bind(PAGE_LAYOUT.eventLogs),
  mainContent: bind(PAGE_LAYOUT.mainContent)
}

const readOnlyComponent = elemConst => console.log(elemConst)

const componentBuild = ({ tag, reactive = {}, content = 'Testing' }) => {
  const dataReact = Object.entries(reactive).map(([data, val]) => `${data}=${val}`).join(' ')

  console.log({ content, dataReact })

  PAGE_LAYOUT.mainContent
    .innerHTML =
      `
        <${tag} ${dataReact}>
          ${content}
        </${tag}>
      `
}

const eventsBuild = evts => {
  templ.eventLogs(PAGE_LAYOUT_BOUND.eventLogs, {
    evts
  })
}

const stateBuild = states => {
  templ.stateProps(PAGE_LAYOUT_BOUND.stateProps, {
    states
  })
}

const reactiveBuild = reacts => {
}

const setupComponent = tag => {
  const customElemConstruct = customElements.get(tag)

  const { observedAttributes = [] } = customElemConstruct

  const currentElem = PAGE_LAYOUT.mainContent.querySelector(`#${tag}`)

  const reactive = Object.values(observedAttributes)
    .reduce((o, react) => {
      if (currentElem)
        o[react] = currentElem.dataset[react] || ''
      else
        o[react] = ''

      return o
    }, {})

  bind(PAGE_LAYOUT.stateProps)`
    <section>
      ${templ.sectionHeader('Edit Content')}

      <textarea onchange='${evt => componentBuild({ tag, reactive, content: evt.target.value })}'>
      </textarea>
    </section>

  `

  if (observedAttributes.length)
    templ.reactiveProps(PAGE_LAYOUT_BOUND.reactiveProps, {
      reacts: observedAttributes,
      change: evt => componentBuild({ tag, reactive, ...evt })
    })
  else
    PAGE_LAYOUT_BOUND.reactiveProps`
      <section>
        ${templ.sectionHeader('No Obsesrvable Propertied')}
      </section>
      `

  componentBuild({ tag })
}

const submitComp = ({ target }) => {
  const compsListOpts = Array.from(PAGE_LAYOUT.compsInput.children, ({ value }) => value)

  if (compsListOpts.includes(target.value))
    setupComponent(target.value)
  else
    console.log('Bad input')
}

const initComponent = optTags => {
  const optionElemStrings = optTags.map(opt =>
    `
      <option value=${opt}>
        ${opt}
      </option>
    ` 
  ).join('\n')

  PAGE_LAYOUT.compsInput
    .insertAdjacentHTML('beforeend', optionElemStrings)
}

if (!MODULE_SCRIPTS)
  throw new Error('No module scripts in header')

const customElementTags = Array.from(
    MODULE_SCRIPTS,
    ({ src }) => customElements.whenDefined(fileName(src)).then(_ => fileName(src))
  )

Promise.all(customElementTags)
  .then(initComponent)

PAGE_LAYOUT.compsInput.addEventListener('change', submitComp)

