import { wire, bind } from 'https://cdn.jsdelivr.net/npm/hyperhtml/esm.js'

const handleReactive = ({ target, val }) => console.log()

const handleState = ({ target, val }) => console.log()

const handleLogs = ({ target, val }) => console.log()

export const sectionHeader = title =>
  wire()`
    <hr>

    <header>
       ${title}
    </header>

    <hr>
  `

export const reactiveProps = (ctx, {
  reacts = [],
  change
}) =>
  ctx`
    <section>
      ${sectionHeader('Reactive Properties')}

      ${
          reacts.map(react =>
            wire()`
              <label>
                ${react}
              </label>

              <input type=text onchange='${evt => change({ reactive: {
                [react]: evt.target.value
              }})}'>
              </input>
            `
          )
      }
    </section>
  `

export const stateProps = (ctx, {
  states,
  target
}) =>
  ctx`
    <section>
      ${sectionHeader('State Properties')}

      ${
        states.map(state =>
          `
            <label>
              ${state}
            </label>

            <input type=text>
            </input>
          `
        )
      }
    </section>
  `

export const eventLogs = (ctx, {
  evts,
  target
}) =>
  ctx`
    <section>
      ${sectionHeader('Events Log')}

      ${
        evts.map(evt =>
          `
            <label>
              ${evt}
            </label>

            <input type=text>
            </input>
          `
        )
      }
    </section>
  `
