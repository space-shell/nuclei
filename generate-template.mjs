import {
  readFileStr,
  writeFileStr
} from "https://deno.land/std/fs/mod.ts"
  
const COMPS_PATH = '/components'

const INDEX_TEMPLATE_FILE = 'src/index.html'

const INDEX_FILE = 'public/index.html'

const SCRIPT_LINE_REPLACE = '<!-- Keep file name the same as the custom element defined tag -->'

;(async () => {
  const indexTemplate = await readFileStr(INDEX_TEMPLATE_FILE, { encoding: "utf8" })

  const files = await Deno.readDir('public/components')

  const fileNameScriptTags = files.map(({ name }) =>
    `<script src=${COMPS_PATH}/${name} type=module></script>`
  ).join('\n')

  const indexNew = indexTemplate.replace(
    SCRIPT_LINE_REPLACE,
    fileNameScriptTags
  )

  writeFileStr(INDEX_FILE, indexNew)
})()
